<?php
/**
 * @file
 * Generics Mocs for Common function of Drupal
 */

/**
 * Mock for drupal_goto
 *
 * Returns an array with the parameters.
 * 
 */
function mock_drupal_goto($path = '', $query = NULL, $fragment = NULL, $http_response_code = 302) {
  return func_get_args();
}