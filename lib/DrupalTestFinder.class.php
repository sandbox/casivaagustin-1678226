<?php

/**
 * Class for find test into modules.
 *
 * @author : Casiva Agustin
 */
class DrupalTestFinder {

  protected $modules = null;
  protected $testFiles = array();

  public function __construct($modules = null) {
    $this->setModules($modules);
  }

  /**
   * Sets modules to tests
   *
   * @param Array $modules : List of module names
   *
   * @throws Exception if there is an invalid module.
   */
  public function setModules($modules) {

    if (empty($modules)) {
      return;
    }

    $this->modules = array();
    //Checks if all modules are valid to test
    foreach ($modules as $module) {
      if (module_exists($module)) {
        $this->modules[] = $module;
      } else {
        throw new Exception("$module is not a valid module to test");
      }
    }
  }

  /**
   * Gets a list of test to run
   *
   * @return array : Path to the test to run
   */
  public function getTests() {
    if (empty($this->testFiles)) {
      $this->findTests();
    }
    return $this->testFiles;
  }

  /**
   * Lookup for test in the modules setted.
   */
  protected function findTests() {
    if (empty($this->modules)) {
      $this->modules = module_list();
    }
    foreach ($this->modules as $module) {
      $this->addModuleTests($module);
    }
  }

  /**
   * Returns the path for the test folder in a module
   *
   * @param String $module : Module Name
   * @return string
   */
  public function getTestPathInModule($module) {
    $module_path = drupal_get_path('module', $module);
    return getcwd() . '/'. $module_path . '/unit_test';
  }

  /**
   * Checks if the Module define some test.
   *
   * @param string $module : The module Name
   * @return boolean
   */
  protected function haveTests($module) {
    if (file_exists($this->getTestPathInModule($module))) {
      return true;
    }
    return false;
  }

  /**
   * Lookup in the module test folder if the module have tests.
   *
   * @param String $module
   */
  protected function addModuleTests($module) {
    if ($this->haveTests($module)) {
      $moduleTestPath = $this->getTestPathInModule($module);
      foreach (glob("$moduleTestPath/*.test.php") as $testFile) {
        $testFile = $testFile;
        $this->testFiles[$testFile] = $testFile;
      }
    }
  }

}