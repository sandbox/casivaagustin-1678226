<?php

class DrupalPHPUnitTestCase extends PHPUnit_Framework_TestCase {

  protected $backupGlobals = FALSE; //Avoids problems with Global Variables.

  /**
   * Loads Admin User
   * @global $user
   */
  protected function setAdminUser() {
    global $user;
    $user = user_load(1);
  }

  /**
   * Loads Anonymous User
   * @global $user
   */
  protected function setAnonymousUser() {
    global $user;
    $user = user_load(0);
  }

  /**
   * Sets up the fixture, for example, open a network connection.
   * This method is called before a test is executed.
   *
   * This restore all the mocked functions, if you need to override this method
   * you must handle the restoring of the functions or call to the parent.
   */
  protected function setUp() {
    DrupalMockFunction::restoreAll(); //Restore All Mocked Functions
  }

  /**
   * Tears down the fixture, for example, close a network connection.
   * This method is called after a test is executed.
   *
   * This restore all the mocked functions, if you need to override this method
   * you must handle the restoring of the functions or call to the parent.
   */
  protected function tearDown() {
    DrupalMockFunction::restoreAll(); //Restore All Mocked Functions
  }
  
}