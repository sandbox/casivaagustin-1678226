<?php

class DrupalMockFunction {

  static protected $counter = 0; //Internal counter for mocked functions
  static protected $currentMocks = array();

  /**
   * Replaces the original function with the mocked function
   *
   * @param String $originalFunction : The function name to replace
   * @param String $mockFunction     : The function to replace with (should be created first).
   */
  public function __construct($originalFunction, $mockFunction) {
    if ( !function_exists( 'rename_function' ) ) {
      trigger_error( 'Test Helper is not installed.', E_USER_ERROR );
    }

    $this->originalFunction = $originalFunction;
    $this->mockFunction = $mockFunction;
    $this->renamedFunction = self::$counter . '_RENAME_' . $this->originalFunction;
    self::$counter++;
    $this->doMock();
  }

  /**
   * Do the mock
   */
  protected function doMock() {
    if (isset($currentMocks[$this->originalFunction])) {
      throw new Exception("Function {$this->originalFunction} already mocked");
    }
    rename_function($this->originalFunction, $this->renamedFunction);
    rename_function($this->mockFunction, $this->originalFunction);
    self::$currentMocks[$this->originalFunction] = $this;
  }

  /**
   * Restore the Original function
   */
  public function restore() {
    rename_function($this->originalFunction, $this->mockFunction);
    rename_function($this->renamedFunction, $this->originalFunction);
    unset(self::$currentMocks[$this->originalFunction]);
  }

  /**
   * Revert all the mock. Use it in tear down for the test suite.
   */
  static public function restoreAll() {
    foreach(self::$currentMocks as $mock) {
      $mock->restore();
    }
    self::$currentMocks = array();
  }

  /**
   * Gets the mock instance for a function if have a mock, Null other wise
   * @param String $functionName
   * @return DrupalMockFunction|NULL The instance of the mock or NULL
   */
  static public function getMockForFunction($functionName) {
    return isset(self::$currentMocks[$functionName])?self::$currentMocks[$functionName]:null;
  }

}