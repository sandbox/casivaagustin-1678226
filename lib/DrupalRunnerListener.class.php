<?php

interface DrupalRunnerListener {

  /**
   * Method Invoked for notify that some event ocurred in the observed object.
   * @param String $event : Event Name
   * @param Array  $data  : Event Data
   */
  public function notify($event, $data = array());

}
