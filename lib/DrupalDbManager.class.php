<?php

class DrupalDbManager implements DrupalRunnerListener {

  public function __construct() {
    module_load_include('module', 'backup_migrate');
    backup_migrate_include('profiles', 'destinations');
  }

  /**
   * Creates a New Backup
   *
   * @return backup_file|boolean|null (Backup and Migrate)
   */
  public function backup() {
    $settings = backup_migrate_get_profile('default');
    $settings->destination_id = 'manual';
    $settings->source_id = 'db';
    ob_start();
    $status = backup_migrate_perform_backup($settings);
    ob_end_clean();
    return $status;
  }

  /**
   * Restore the Last Backup
   *
   * @return backup_file|boolean|null (Backup and Migrate)
   */
  public function restore() {
    $bkpFile = $this->getLastBackup();
    $settings = array('source_id' => 'db');
    ob_start();
    $status = backup_migrate_perform_restore('manual', $bkpFile, $settings);
    ob_end_clean();
    return $status;
  }

  /**
   * Gets the full path to the last Backup
   *
   * @return String
   */
  public function getLastBackupPath() {
    return $this->getBackupsPath() . '/' . $this->getLastBackup();
  }

  /**
   * Gets the Path where the Backups are saved
   *
   * @return String
   */
  public function getBackupsPath() {
    $destination = backup_migrate_get_destination('manual');
    return getcwd() . '/' . $destination->location;
  }

  /**
   * Gets the Last Backup
   *
   * @return String : Gets the Last Backup File Name
   */
  public function getLastBackup() {
    $bkpFileName = null;
    $last = array_pop($this->getBackups());
    if ($last) {
      $info = $last->info();
      $bkpFileName = $info['filename'];
    }
    return $bkpFileName;
  }

  /**
   * Gets a list of available backups
   *
   * @return Array : File names of availables backups
   */
  public function getBackups() {
    $bkps = array();
    $destination = backup_migrate_get_destination('manual');
    $files = $destination->list_files();
    foreach($files as $file) {
      if ($file->is_recognized_type()) {
        $bkps[] = $file;
      }
    }
    return $bkps;
  }

  /**
   * {@inheritdoc}
   */
  public function notify($event, $data = array()) {
    if ($event === DrupalTestRunner::EVENT_PRERUN) {
      $this->backup();
    }
    if ($event === DrupalTestRunner::EVENT_POSTRUN) {
      $this->restore();
    }
  }

}