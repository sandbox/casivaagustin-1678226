<?php

require_once 'PHPUnit/Autoload.php'; //PHPUnit should be installed
require_once dirname(__FILE__) . '/DrupalRunnerListener.class.php';
require_once dirname(__FILE__) . '/DrupalDbManager.class.php';
require_once dirname(__FILE__) . '/DrupalTestFinder.class.php';
require_once dirname(__FILE__) . '/DrupalResultPrinter.class.php';
require_once dirname(__FILE__) . '/DrupalCoverage.class.php';
require_once dirname(__FILE__) . '/DrupalPHPUnitTestCase.class.php';
require_once dirname(__FILE__) . '/DrupalMockFunction.class.php';
require_once dirname(__FILE__) . '/mocks/common.inc';

class DrupalTestRunner {

  const EVENT_PRERUN       = 'PRERUN';
  const EVENT_POSTRUN      = 'POSTRUN';
  const EVENT_RUNSTARTS    = 'RUNSTARTS';
  const EVENT_RUNENDS      = 'RUNENDS';
  static private $instance = null;

  public function __construct($modules, $options) {
    $this->testFiles  = array();
    $this->observers  = array();
    $this->verbose    = isset($options['verbose'])?$options['verbose']:false;
    $this->backup     = isset($options['backup'])?$options['backup']:false;
    $this->coverage   = isset($options['coverage'])?$options['coverage']:false;
    $this->json       = isset($options['json'])?$options['json']:false;
    $this->junit      = isset($options['junit'])?$options['junit']:false;
    $this->testFinder = new DrupalTestFinder($modules);
    $this->printer    = new DrupalResultPrinter(null, $this->verbose, true, $this->verbose);
    $this->results    = new PHPUnit_Framework_TestResult();
    $this->results->addListener($this->printer);
    $this->setOutputPath(isset($options['output'])?$options['output']:null);
    $this->attachDbManager();
    $this->attachCoverage();
    $this->attachJUnit();
    $this->attachJson();
  }

  /**
   * Gets the instance of the current Runner Process
   *
   * @param Array $modules : Runner Modules to find tests
   * @param Array $options : Runner Options
   *
   * @return DrupalTestRunner
   */
  static public function getInstance($modules = null, $options = array()) {
    if (!isset(self::$instance)) {
      self::$instance = new DrupalTestRunner($modules, $options);
    }
    return self::$instance;
  }

  /**
   * Runs the Test Suite
   *
   * @return boolean
   */
  public function run() {
    $this->notifyToObservers(self::EVENT_RUNSTARTS);
    $testSuite = $this->createTestSuite();
    if ($testSuite->count() > 0) {
      $this->notifyToObservers(self::EVENT_PRERUN);
      $testSuite->run($this->results);
      $this->notifyToObservers(self::EVENT_POSTRUN);
    }
    $this->printer->printResult($this->results);
    $this->results->flushListeners();
    $this->notifyToObservers(self::EVENT_RUNENDS);
    return true;
  }

  /**
   * Creates the Test Suite
   *
   * @return PHPUnit_Framework_TestSuite
   */
  public function createTestSuite() {
    $testSuite = new PHPUnit_Framework_TestSuite();
    $tests = $this->testFinder->getTests();
    $testSuite->addTestFiles($tests);
    return $testSuite;
  }

  /**
   * Add a Observer
   *
   * @param String $event      : Type of Event for Suscribe
   * @param Listener $observer : An Observer Object
   */
  public function addListener($event, DrupalRunnerListener $observer) {
    $this->observers[$event][] = $observer;
  }

  /**
   * Send notifications to the Observers
   *
   * @param String $event : Type of event.
   */
  protected function notifyToObservers($event) {
    if (isset($this->observers[$event])) {
      foreach($this->observers[$event] as $observer) {
        $observer->notify($event, array('runner' => $this));
      }
    }
  }

  /**
   * Attachs the DbManager in order to create Backups before and after the process.
   * Needs the option backup.
   */
  protected function attachDbManager() {
    if ($this->backup) {
      $this->dbManager = new DrupalDbManager();
      $this->addListener(self::EVENT_PRERUN, $this->dbManager);
      $this->addListener(self::EVENT_POSTRUN, $this->dbManager);
    }
  }

  /**
   * Attachs to results the Listener in order to generate Coverage Reports.
   * Needs the option coverage.
   */
  protected function attachCoverage() {
    if ($this->coverage) {
      $this->drupalCoverage = new DrupalCoverage();
      $this->results->setCodeCoverage($this->drupalCoverage->codeCoverage);
      $this->addListener(self::EVENT_POSTRUN, $this->drupalCoverage);
    }
  }

  /**
   * Attachs to results the Listener in order to generate Json Logs.
   * Needs the option json.
   */
  protected function attachJson() {
    if ($this->json) {
      $this->results->addListener(new PHPUnit_Util_Log_JSON($this->getOutputPath() . '/log.json'));
    }
  }

  /**
   * Attachs to results the Listener in order to generate JUnit Logs.
   * Needs the option junit.
   */
  protected function attachJUnit() {
    if ($this->junit) {
      $junit = new PHPUnit_Util_Log_JUnit($this->getOutputPath() . '/log.xml');
      $junit->setWriteDocument(true);
      $this->results->addListener($junit);
    }
  }

  /**
   * Gets the current Output Path
   *
   * @return String
   */
  public function getOutputPath() {
    if (empty($this->output)) {
      return getcwd() . '/' . drupal_realpath('public://');
    } else {
      return $this->output;
    }
  }

  /**
   * Sets the Output path for the reports and logs
   *
   * @param String $path : The output path.
   */
  public function setOutputPath($path) {
    $this->output = isset($path)?$path:false;
    if ($this->output) {
      if (!file_exists($this->output)) {
        mkdir($this->output, 0777, true);
      }
    }
  }

}