<?php

class DrupalCoverage implements DrupalRunnerListener {

  public function __construct() {
    $coverageFilter = new PHP_CodeCoverage_Filter;
    $coverageFilter->addDirectoryToBlacklist('/Applications/MAMP/bin/php');
    $coverageFilter->addDirectoryToBlacklist(getcwd() , '.test.php');
    $this->codeCoverage = new PHP_CodeCoverage(
      NULL, $coverageFilter
    );
    $this->codeCoverage->setCacheTokens(true);
    $this->writer = new PHP_CodeCoverage_Report_HTML(
        'Coverage', 'UTF-8', false, true, true, true,
        ' and PHPUnit ' . PHPUnit_Runner_Version::id()
    );
    ini_set('memory_limit', -1); //Unlimited.
  }

  /**
   * {@inheritdoc}
   */
  public function notify($event, $data = array()) {
    if ($event === DrupalTestRunner::EVENT_POSTRUN) {
      $this->createCoverageReport();
    }
  }

  /**
   * Creates the Coverage Report in HTML Format.
   */
  protected function createCoverageReport() {
    $this->runner = DrupalTestRunner::getInstance();
    $this->runner->printer->write("\nGenerating code coverage report in HTML format");
    $this->writer->process($this->codeCoverage, $this->runner->getOutputPath() . '/test_report/');
    $this->runner->printer->write("; Done!\n");
  }

}
