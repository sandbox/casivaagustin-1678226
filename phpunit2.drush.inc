<?php
/**
 * @file
 *
 * Drush Commands for PHPUnit2
 *
 */
require_once dirname(__FILE__) . '/lib/DrupalTestRunner.class.php';

/**
 * Implementation of hook_drush_command().
 */
function phpunit2_drush_command() {
  $items['phpunit-run'] = array(
    'callback' => 'phpunit2_run',
    'description' => 'Run Tests',
    'bootstrap'   => 'DRUSH_BOOTSTRAP_MAX',
    'options' => array(
      'modules'  => 'The List of coma separated Modules Names to Test, by default will run the test in all modules',
      'backup'   => 'Yes to make a backup before start the test and recover when is completed, default false.',
      'coverage' => 'Generate Coverage Report, HTML',
      'json'     => 'Generate Json Log',
      'junit'    => 'Generate jUnit Log',
      'output'   => 'Path to the Output Directory, where the logs and report will be generated'
    ),
    'aliases' => array('pur'),
  );
  return $items;
}

/**
 * Prepare and Runs the Tests
 */
function phpunit2_run() {
  $modules = phpunit2_parse_modules(drush_get_option('modules', NULL));
  $options = phpunit2_parse_options();
  try {
    $drupal_unit = DrupalTestRunner::getInstance($modules, $options);
    $drupal_unit->run();
  }catch(Exception $e) {
    drush_set_error($e->getMessage());
  }
}

/**
 * Parse the Command line Options
 *
 * @return Array
 */
function phpunit2_parse_options() {
  $options = array();
  $options['verbose']  = (bool) drush_get_context('DRUSH_VERBOSE');
  $options['backup']   = (bool) drush_get_option('backup', FALSE);
  $options['coverage'] = (bool) drush_get_option('coverage', FALSE);
  $options['json']     = (bool) drush_get_option('json', FALSE);
  $options['junit']    = (bool) drush_get_option('junit', FALSE);
  $options['output']   = drush_get_option('output', FALSE);
  return $options;
}

/**
 * Parses the List of Modules
 *
 * @param String $modules : List with comma separated modules
 *
 * @return Array
 */
function phpunit2_parse_modules($modules) {
  if ($modules !== NULL) {
    $modules = explode(',', $modules);
  }
  return $modules;
}