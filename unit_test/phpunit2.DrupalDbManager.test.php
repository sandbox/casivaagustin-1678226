<?php

class PhpunitDrupalDbManagerTest extends DrupalPHPUnitTestCase
{
    public function notForNow_testBackup()
    {
      $dbManger = new DrupalDbManager();
      ob_start();
      $result = @$dbManger->backup();
      ob_end_clean();
      $this->assertNotNull($result, 'The Backup has Fail');
      $this->assertNotNull($dbManger->getBackups(), 'Cant Get any Backup');
      $lastBackup = $dbManger->getLastBackup();
      $this->assertNotNull($lastBackup, 'The Last Bkp');
      $lastBackupPath = $dbManger->getLastBackupPath();
      $this->assertTrue(file_exists($lastBackupPath), 'The Backup is not there');
      if ($result) {
        ob_start();
        $result = $dbManger->restore();
        ob_end_clean();
        $this->assertNotNull($result, 'The cant recovery the Backup');
        @unlink($lastBackup);
      }
    }

    public function testNotify() {
      $dbManagerMock = $this->getMock('DrupalDbManager', array('backup', 'restore'));

      $dbManagerMock->expects($this->any())
            ->method('backup')
            ->will($this->returnValue(true));

      $dbManagerMock->expects($this->any())
            ->method('restore')
            ->will($this->returnValue(true));

      $dbManagerMock->notify(DrupalTestRunner::EVENT_POSTRUN);
      $dbManagerMock->notify(DrupalTestRunner::EVENT_PRERUN);

    }

}