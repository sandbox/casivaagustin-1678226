<?php

class PhpunitDrupalCoverage extends DrupalPHPUnitTestCase {

  public function testCoverage() {
    $coverage = new DrupalCoverage();
    $this->assertInstanceOf('DrupalCoverage', $coverage, 'Should be a Drupal Coverage');
    $this->assertInstanceOf('DrupalRunnerListener', $coverage, 'Should implements DrupalRunnerListener');

    //Mocks of the Report Generator
    $writerMock = $this->getMock('PHP_CodeCoverage_Report_HTML', array('process'));
    $writerMock->expects($this->once())
        ->method('process')
        ->will($this->returnValue(true));

    $coverage->writer = $writerMock; //Change with the mocked class
    ob_start();
    $coverage->notify(DrupalTestRunner::EVENT_POSTRUN);
    $output = ob_get_clean();
    $this->assertEquals($output, "\nGenerating code coverage report in HTML format; Done!\n", 'Bad Message after generate coverage');
  }

  public function testNotify() {
    $coverage = $this->getMock('DrupalCoverage', array('createCoverageReport'));
    $coverage->expects($this->once())
            ->method('createCoverageReport')
            ->will($this->returnValue(true));
    $coverage->notify(DrupalTestRunner::EVENT_POSTRUN);
  }

}