<?php

class PhpunitDrupalMockFunctions extends DrupalPHPUnitTestCase {

  public function testMockFunction() {
    $this->assertEquals(original_function(), 'original');
    $mockedFunction  = new DrupalMockFunction('original_function', 'mock_original_function');
    $this->assertEquals(original_function(), 'mocked');
    $mockInstance = DrupalMockFunction::getMockForFunction('original_function');
    $this->assertEquals($mockInstance, $mockedFunction);
    $mockedFunction->restore();
    $this->assertEquals(original_function(), 'original');
  }

}

function original_function() {
  return 'original';
}

function mock_original_function() {
  return 'mocked';
}