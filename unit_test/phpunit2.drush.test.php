<?php

class PhpUnitDrushTest extends PHPUnit_Framework_TestCase
{
    public function testPhpunitDrushCommand()
    {
      $command = phpunit2_drush_command();
      $this->assertTrue(isset($command['phpunit-run']), 'Run entry is not setted');
      $this->assertTrue(function_exists($command['phpunit-run']['callback']), 'The callback do not exists');
    }

    public function testModuleOptionParser()
    {
      $modules = phpunit2_parse_modules(null);
      $this->assertNull($modules, 'Must be null');
      $modules = phpunit2_parse_modules('uno,dos,tres');
      $this->assertCount(3, $modules, 'Must be 3');
    }

    public function testParseOptions()
    {
      $options = phpunit2_parse_options();
      $this->assertTrue(is_array($options), 'Should be an array');
    }
}