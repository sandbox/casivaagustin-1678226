<?php

class PhpunitDrupalTestFinderTest extends DrupalPHPUnitTestCase
{
    public function testFinder()
    {
      $tf = new DrupalTestFinder(array('phpunit2'));
      $finderFoundTests = $tf->getTests();
      $testPath = $tf->getTestPathInModule('phpunit2');
      $localFoundTests = array();
      foreach (glob("$testPath/*.test.php") as $testFile) {
        $localFoundTests[] = $testFile;
      }
      $this->assertCount(count($finderFoundTests), $localFoundTests, 'The amount of founded test is not the same');
      $currentTestFile = $tf->getTestPathInModule('phpunit2') . '/' . basename(__FILE__);
      $this->assertContains($currentTestFile, $finderFoundTests, 'Cant find the current test in the list of tests');

      $tf->setModules(null);
      $tests = $tf->getTests();
      $this->assertTrue(is_array($tests), 'Is not an array');
    }

    public function testEmptyModules() {
      $tf = new DrupalTestFinder();
      $test = $tf->getTests();
      $this->assertNotNull($test);
    }

}