<?php

class PhpunitDrupalTestRunner extends DrupalPHPUnitTestCase {

  public function testRunner() {
    if (!module_exists('phpunit2_mock')) {
      DrupalTestRunner::getInstance()->printer->write("\n!!! phpunit2_mock must be enabled, some test wont run !!!\n");
      return ;
    }

    $runner = new DrupalTestRunner(array('phpunit2_mock'), array('backup' => true, 'verbose' => false));
    $this->assertInstanceOf('DrupalResultPrinter', $runner->printer);
    $this->assertInstanceOf('PHPUnit_Framework_TestResult', $runner->results);
    $this->assertInstanceOf('DrupalTestFinder', $runner->testFinder);

    $runner = new DrupalTestRunner(array('phpunit2_mock'), array(
        'backup' => false, 'verbose' => false, 'coverage' => true,
        'json' => true, 'junit' => true)
    );

    // Test if the runner works
    $dbManagerMock = $this->getMock('DrupalDbManager', array('backup', 'restore'));
    $dbManagerMock->expects($this->any())
            ->method('backup')
            ->will($this->returnValue(true));
    $dbManagerMock->expects($this->any())
            ->method('restore')
            ->will($this->returnValue(true));

    $runner->dbManager = $dbManagerMock;

    ob_start();
    $status = $runner->run();
    ob_end_clean();

    $this->assertTrue($status, 'Runner throw some error');
  }

  public function testRunnerConstruct() {
    $runner = new DrupalTestRunner(array('phpunit2_mock'), array('backup' => true, 'verbose' => false));
    $this->assertInstanceOf('DrupalTestRunner', $runner, 'Should be a Test Runner');
  }

  public function testOutputPath() {
    $runner = new DrupalTestRunner(array('phpunit2_mock'), array('backup' => true, 'verbose' => false));

    //Default Directory Path
    $this->assertEquals(getcwd() . '/' . drupal_realpath('public://'), $runner->getOutputPath());

    //Alternative Path
    $tempWorkDir = sys_get_temp_dir() . '/testTemp';
    if (file_exists($tempWorkDir)) {
        rmdir($tempWorkDir);
    }
    $runner->setOutputPath($tempWorkDir);
    $this->assertTrue(file_exists($tempWorkDir), "The output folder $tempWorkDir is not created");
    $this->assertEquals($tempWorkDir, $runner->getOutputPath());

    //Back to Default
    $runner->setOutputPath(null);
    $this->assertEquals(getcwd() . '/' . drupal_realpath('public://'), $runner->getOutputPath());
  }

  public function testNotify() {
    $testSuiteMock = $this->getMock('PHPUnit_Framework_TestSuite');

    $testSuiteMock->expects($this->any())
            ->method('run')
            ->will($this->returnValue(true));

    $testSuiteMock->expects($this->any())
            ->method('count')
            ->will($this->returnValue(1));

    $runner = $this->getMock('DrupalTestRunner', array('createTestSuite'),
            array(
                array('phpunit2_mock'), array('backup' => false, 'verbose' => false)
            )
    );

    $runner->expects($this->any())
            ->method('createTestSuite')
            ->will($this->returnValue($testSuiteMock));

    $mockListener = new MockListener($this);
    $runner->addListener(DrupalTestRunner::EVENT_RUNSTARTS, $mockListener);
    ob_start();
    $runner->run();
    $output = ob_get_clean();
  }

}

class MockListener implements DrupalRunnerListener {

  public function __construct(PHPUnit_Framework_TestCase $testInstance) {
    $this->testInstance = $testInstance;
  }

  public function notify($event, $data = array()) {
    $this->testInstance->assertEquals($event, DrupalTestRunner::EVENT_RUNSTARTS);
    $this->testInstance->assertTrue(is_array($data));
  }

}