PHPUnit 2

Objetive

Provides Unit test to Drupal Modules unsing PHPUnit. This modules provides a custom
runner to run PHPUnit test in Drupal.
Also include some features to mock functions using the test_helper extension created
by Sebastian Bergman.

Instalation

Previous to the module instalation you need to have installed PHPUnit, Xdebug and Test_Helpers
extension.

In order to install PHPUnit do the following steps

  pear channel-discover pear.phpunit.de
  pear channel-discover components.ez.no
  pear channel-discover pear.symfony-project.com
  pear upgrade PEAR
  pear install phpunit/PHPUnit

Install Xdebug in Debian (or debians likes).

  apt-get install php5-xdebug

Install Test Helpers

  pecl install phpunit/test_helpers

At the end include the extension in your php.ini

  zend_extension=/path_to_the_extension/test_helpers.so

Test if the extension was loaded

  $php --info|grep "test_helpers support"
  test_helpers support => enabled

Once you have all that installed you can proceed with the instalation of this module as
any drupal module.

  Copy the module in some reacheable location by your drupal site.

  drush en phpunit2 -y

This module depends on backup_migrate, so drush will try to download it but you can do it
manually if you want.

Create some test

First. You have to know how use PHPUnit, take a look into PHPUnit Documentation.

If you want to create a test for a module you have to create a folder into the root of the module called "unit_test".
Inside of that folder you have to create the phpunit TestCases classes with this convention for the class name.

  {module_name}.{className}.test.php

This module will search in all enabled modules for unit test classes with that pattern and will
try to execute the requested test.

The test must be a class that extends from DrupalPHPUnitTestCase, something like this.

  <?php
    class {ModuleName}{ClassName} extends DrupalPHPUnitTestCase {

      public function test<TestName>() {
        .. the test body goes here ..
      }

      .... more tests ..
    }

For more advances techniques for test creation check the PHPUnit Documentation.

Mocking Functions

Drupal have a lot of core function, this function are a problem in the Unit testing because is
hard mock them. In order to mock this functions this module uses test_helpers, this extension
allow rename a function, if we rename a function we can replace the original function with a mock.

We included a custom class to make the function mocking and for handle the restore of the mocked
functions after the test.

In order to mock a function, by example drupal_goto, you have to create the function mock first, lets
call to the mock mock_drupal_goto, and after that create a instance of DrupalMockFunction like this.

Create a Mock function
  ...
  function mock_drupal_goto($path = '', $query = NULL, $fragment = NULL, $http_response_code = 302) {
   ... code a mock ...
  }
  ...

In the test method

  $mock = new DrupalMockFunction('drupal_goto', 'mock_drupal_goto'); //Replace the functions

  ... do the test ...

  $mock->restore(); // Restore the original function.

Thats it.

Run test

The test must be lauched with drush, the easy easy way is.

  drush pur

If you need more information

  drush pur -v

If you need to test some specific module

  drush pur -v --modules=module1,module2,module3

If you wan't to backup the database before run the test and recover the db when the test suite finish.

  drush pur -v --backup=y

If you need to get a log of the process

  drush pur -v --outpur=/some/path --json=y --junit=y

Coverage

If you need a coverage report

  drush pur -v --outpur=/some/path --coverage=y

Thats its Folks, happy testing.